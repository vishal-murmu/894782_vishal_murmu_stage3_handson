$(document).ready(function() {

    $("span").mousemove(function(){
        if($(this).text().match(/th/)){
            $(this).css({
                "font-style": "italic",
                "text-shadow": "2px 2px pink",
                "font-weight":"bolder"
            })
        }

        $(this).css({
            "background-color": "bisque"
        })
    });

    $("span").mouseout(function(){
        $(this).css({
            "background-color": "transparent"
        })
    });

});